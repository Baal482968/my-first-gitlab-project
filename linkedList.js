// constructor of linkedlist
class LinkedList{
  constructor(head, tail){
    this.head = null;
    this.tail = null;
  }
}
// constructor of node
class Node{
  constructor(value, prev, next){
    this.value = value;
    this.prev = prev;
    this.next = next;
  }
}
// add node front method
LinkedList.prototype.addToFront = function(value){
  const newNode = new Node(value, null, this.head);
  if(this.head){
    this.head.prev = newNode;
  }
  else{
    this.tail = newNode;
  }
  this.head = newNode;
}
// add node end method
LinkedList.prototype.addToEnd = function(value){
  const newNode = new Node(value, this.tail, null);
  if(this.tail){
    this.tail.next = newNode;
  }
  else{
    this.head = newNode;
  }
  this.tail = newNode;
}
// delete node front method
LinkedList.prototype.deleteHead = function(){
  if(!this.head) return null;
  let value = this.head.value;
  this.head = this.head.next;
  if(this.head){
    this.head.prev = null;
  } 
  else{
    this.tail = null;
  }
  return value;
}
// delete node end method
LinkedList.prototype.deleteTail = function(){
  if(!this.tail) return null;
  let value = this.tail.value;
  this.tail = this.tail.prev;
  if(this.tail){
    this.tail.next = null;
  }
  else{
    this.head = null;
  }
  return value;
}
// search method
LinkedList.prototype.searchNode = function(value){
  let currentNode = this.head;
  while(currentNode){
    if(currentNode.value === value){
      return currentNode.value;
    }
    else{
      currentNode = currentNode.next;
    }
  }
  return null;
}


const list = new LinkedList();
list.addToFront(123);
list.addToFront(456);
list.addToEnd(1010);
list.addToEnd("BaalWu");
list.addToEnd(789);
console.log("This is origin list "+list);

// var rh = list.deleteHead();
// var rt = list.deleteTail();

// console.log("We remove head:"+rh+", and remove tail:"+rt);
// console.log("This is list after delete:"+list);
console.log(list.searchNode(456));
console.log(list.searchNode(101010));
console.log(list.searchNode("BaalWu"));


