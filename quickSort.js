var array = [5,7,1,1,0,9,10,3,5,4]

var partition = (arr, left, right) => {
  var pivot = arr[right];
  var i = left-1;
  for(let j = left; j < right; j++){
    if(arr[j] <= pivot){
      i++;
      swap(arr, i, j);
    }
  }
  swap(arr, i+1, right);
  return i+1;
}

var quickSort = (arr, left, right) => {
  if(left < right){
    let pi = partition(arr, left, right);
    quickSort(arr, left, pi-1);
    quickSort(arr, pi+1, right);
  }
}

var swap = (arr, a, b) => {
  let temp = arr[a];
  arr[a] = arr[b];
  arr[b] = temp;
}

quickSort(array,0,array.length-1)

console.log(array);

