var array = [5,7,1,1,0,9,10,3,5,4]

function bubbleSort(arr){
  var sorted = false;
  var n = arr.length;
  while(!sorted){
    sorted = true;
    for(var i = 0; i < n-1; i++){
      if(arr[i] > arr[i+1]){
        let temp = arr[i];
        arr[i] = arr[i+1];
        arr[i+1] = temp;
        sorted = false;
      }
    }
    n--;
  }
  return arr;
}

var newArray = bubbleSort(array);
console.log(newArray);