var array = [5,7,1,1,0,9,10,3,5,4];

const selectionSort = (arr) => {
  for(let i = 0; i < arr.length; i++){
    let min = i;
    for(let j = i+1; j < arr.length; j++){
      if(arr[min]>arr[j]){
        min = j;
      }
    }
    let temp = arr[i];
    arr[i] = arr[min];
    arr[min] = temp;
  }
  return arr;
}

console.log(selectionSort(array));
