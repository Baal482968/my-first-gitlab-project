class BinarySearchTree {
  constructor(){
    this.root = null;
  }
}

class Node {
  constructor(val){
    this.val = val;
    this.left = null;
    this.right = null;
  }
}

BinarySearchTree.prototype.push = (val) => {
  var root = this.root;
  if(!root){
    this.root = new Node(val);
    return;
  }

  var currentNode = root;
  var newNode = new Node(val);
  
  while(newNode){
    if(newNode > currentNode){
      if(!currentNode.left){
        currentNode.left = newNode;
        break;
      }
      else{
        currentNode = currentNode.left;
      }
    }
    else{
      if(!currentNode.right){
        currentNode.right = newNode;
        break;
      }
      else{
        currentNode = currentNode.right;
      }
    }
  }
}

var bst = new BinarySearchTree();
bst.push(5)